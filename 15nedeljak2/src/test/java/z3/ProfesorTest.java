/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z3;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.ArgumentCaptor;
import static org.mockito.ArgumentMatchers.argThat;
import org.mockito.InOrder;
import org.mockito.Mockito;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author minag
 */
public class ProfesorTest {
    private Profesor profesor;
    private Asistent asistent;
    private Student student;
    
    public ProfesorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        this.profesor = new Profesor();
        this.asistent = mock(Asistent.class);
        this.profesor.setAsistent(asistent);
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testOdrziIspit() {
        System.out.println("odrziIspit");
        this.student = new Student("1234", "Pera Peric", 6);
        this.profesor.odrziIspit(this.student);
        StudentMatcher s = new StudentMatcher(this.student.getBrojIndeksa());
        ArgumentCaptor<Student> st = ArgumentCaptor.forClass(Student.class);
        verify(this.asistent, times(1)).prviKolokvijum(st.capture());
        verify(this.asistent, times(1)).drugiKolokvijum(st.capture());
        verify(this.asistent, times(1)).odbranaProjekta(st.capture());
        
//        InOrder testOrder = inOrder(this.asistent);
//        testOrder.verify(this.asistent).prviKolokvijum(student);
//        testOrder.verify(this.asistent).drugiKolokvijum(student);
//        testOrder.verify(this.asistent).odbranaProjekta(student);
        
//        verify(this.asistent, atLeast(1)).prviKolokvijum(argThat(s));
//        verify(this.asistent, times(1)).drugiKolokvijum(argThat(s));
//        verify(this.asistent, times(1)).odbranaProjekta(argThat(s));
       
        for(Student c : st.getAllValues()){
            assertEquals(st.getValue().getBrojIndeksa(), c.getBrojIndeksa());
        }
        
    }

    
}
