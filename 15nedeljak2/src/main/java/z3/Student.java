/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z3;

/**
 *
 * @author Mladen
 */
public class Student {
    
    private String brojIndeksa;
    private String ime;
    private int ocena;

    public String getBrojIndeksa() {
        return brojIndeksa;
    }

    public void setBrojIndeksa(String brojIndeksa) {
        this.brojIndeksa = brojIndeksa;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public int getOcena() {
        return ocena;
    }

    public void setOcena(int ocena) {
        this.ocena = ocena;
    }
    
    public Student(){
        
    }
    
    public Student(String brInd, String ime, int ocena){
        this.brojIndeksa = brInd;
        this.ime = ime;
        this.ocena = ocena;
    }
    
}
