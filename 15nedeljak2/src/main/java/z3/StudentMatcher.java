/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z3;

/**
 *
 * @author minag
 */

import z3.Student;
import org.mockito.ArgumentMatcher;
public class StudentMatcher implements ArgumentMatcher<Student>{
    
    private final String brojIndexa;
    
    public StudentMatcher(String brojIndexa){
        this.brojIndexa = brojIndexa;
    }

    @Override
    public boolean matches(Student t) {
        return t.getBrojIndeksa().equals(this.brojIndexa);
    }
    
    
    
}
