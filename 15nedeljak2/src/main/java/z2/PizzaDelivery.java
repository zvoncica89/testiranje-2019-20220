/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z2;

/**
 *
 * @author Mladen
 */
public class PizzaDelivery {
    
    public float calculatePrice(int numberOfPizzas, int hours, int distance){
        float price = 100;
        
        if(distance > 50 && distance < 100){
            price *=2;
            if(hours > 23 || hours < 6){
                price += 500;
            }
        }
        
        else if(distance >= 100){
            price *=3;
            if(hours > 23 || hours < 6){
                price += 600;
            }
        }
        else{
            if(hours > 23 || hours < 6){
                price = 300;
            }
        }
        
        if(price > 1000){
            price *=0.9;
        }
        
        return price * numberOfPizzas;
    }
    
}
