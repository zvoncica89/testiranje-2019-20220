/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z1;

/**
 *
 * @author Mladen
 */
public class VideoGame {
    
    public static enum WeaponType{Spear, Sword, Dagger};
    public static enum ArmorType{Light, Medium, Heavy};
    
    public float calculateDamageMultiplier(int playerHP, int enemyHP, WeaponType weapon, ArmorType armor){
        float multiplier = 1;
        
        if(playerHP <= 50){
            multiplier += 1;
        }
        if(playerHP <25){
            multiplier += 2;
        }
        
        if(weapon == WeaponType.Spear){
            if(armor == ArmorType.Heavy){
                multiplier =2;
            }
            if(armor == ArmorType.Light){
                multiplier *=0.5;
            }
            if(armor == ArmorType.Medium){
                multiplier *= 1;
            }
        }
        
        if(weapon == WeaponType.Sword){
            if(armor == ArmorType.Heavy){
                multiplier *=0.5;
            }
            if(armor == ArmorType.Light){
                multiplier *=1;
            }
            if(armor == ArmorType.Medium){
                multiplier *= 1;
            }
        }
        
        if(weapon == WeaponType.Dagger){
            if(armor == ArmorType.Heavy){
                multiplier *=2;
            }
            if(armor == ArmorType.Light){
                multiplier *=2;
            }
            if(armor == ArmorType.Medium){
                multiplier *= 0.5;
            }
        }
        
        return multiplier;
    }
    
}
